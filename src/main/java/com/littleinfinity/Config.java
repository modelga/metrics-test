package com.littleinfinity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.littleinfinity.config.AppConfig;
import com.littleinfinity.config.DefaultAppConfig;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = "com.littleinfinity")
public class Config {

	@Bean
	public AppConfig appConfig() {
		return new DefaultAppConfig();
	}
}
