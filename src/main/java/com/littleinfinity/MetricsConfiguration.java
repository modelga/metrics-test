package com.littleinfinity;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.codahale.metrics.MetricRegistry;
import com.littleinfinity.config.AppConfig;
import com.littleinfinity.metrics.DatadogMetricsReporter;
import com.littleinfinity.metrics.DatadogMetricsReporter.Builder;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;

@Configuration
@EnableMetrics(exposeProxy = true, proxyTargetClass = true)
public class MetricsConfiguration extends MetricsConfigurerAdapter {

	@Autowired
	private AppConfig config;

	@Autowired
	MetricRegistry metricRegistry;

	@PostConstruct
	public void init() {
		configureReporters(metricRegistry);
	}

	@Override
	public void configureReporters(MetricRegistry metricRegistry) {
		Builder reporterBuilder = DatadogMetricsReporter.newBuilder(this.metricRegistry).withApiKey(config.getDatadogAPIKey())
				.withAppKey(config.getDatadogAppKey());
		reporterBuilder.build().start(3, TimeUnit.SECONDS);
	}

}
