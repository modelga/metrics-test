package com.littleinfinity.config;

import com.littleinfinity.request.BoundingBox;

public interface AppConfig {

	BoundingBox getBoundingBox();

	String getWeatherKey();

	String getDatadogAPIKey();

	String getDatadogAppKey();

}
