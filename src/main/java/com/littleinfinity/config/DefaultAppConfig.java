package com.littleinfinity.config;

import com.littleinfinity.request.BoundingBox;

public class DefaultAppConfig implements AppConfig {

	@Override
	public BoundingBox getBoundingBox() {
		return new BoundingBox(18.2792, 49.6774, 19.5172, 50.7225);
	}

	@Override
	public String getWeatherKey() {
		return "9772cb6abf957617bab3dd45313cb063";
	}

	@Override
	public String getDatadogAPIKey() {
		return "348ba2fd1ebae89cc1630e400b66fe55f4f9f35a";
	}

	@Override
	public String getDatadogAppKey() {
		return "249149ad873b348bc95d62f97d3056b9";
	}

}
