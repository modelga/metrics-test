package com.littleinfinity.metrics;

import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;
import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Timer;
import com.google.common.collect.Maps;

public class DatadogMetricsReporter extends ScheduledReporter {

	private static String URL = "https://app.datadoghq.com/api/v1/{type}?api_key={apiKey}&application_key={appKey}";

	private final String apiKey;
	private final String appKey;
	private final RestTemplate rest;

	private DatadogMetricsReporter(MetricRegistry registry, String name, MetricFilter filter, TimeUnit rateUnit, TimeUnit durationUnit, String apiKey,
			String appKey) {
		super(registry, name, filter, rateUnit, durationUnit);
		this.apiKey = apiKey;
		this.appKey = appKey;
		MappingJackson2HttpMessageConverter jackson = new MappingJackson2HttpMessageConverter();
		jackson.getObjectMapper().configure(FAIL_ON_EMPTY_BEANS, false);
		jackson.getObjectMapper().configure(FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS, false);
		jackson.getObjectMapper().configure(WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED, true);
		rest = new RestTemplate(newArrayList(jackson, new StringHttpMessageConverter()));
	}

	@Override
	public void report(SortedMap<String, Gauge> gauges, SortedMap<String, Counter> counters, SortedMap<String, Histogram> histograms,
			SortedMap<String, Meter> meters, SortedMap<String, Timer> timers) {
		try {
			showReport(meters, timers, gauges);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static class Series {
		String metric;
		Object[][] points;
		String host;

		public String getMetric() {
			return metric;
		}

		public void setMetric(String metric) {
			this.metric = "java.weather.".concat(metric.replace(' ', '.'));
		}

		public Object[][] getPoints() {
			return points;
		}

		public void setPoints(Object[][] points) {
			this.points = points;
		}

		public String getHost() {
			return "springbay";
		}
	}

	private void showReport(SortedMap<String, Meter> meters, SortedMap<String, Timer> timers, SortedMap<String, Gauge> gauges) {
		if (timers.size() > 0) {
			for (Entry<String, Timer> timer : timers.entrySet()) {
				System.out.println(String.format("%10s is %f", timer.getKey(), timer.getValue().getOneMinuteRate()));
			}
		}
		if (meters.size() > 0) {
			for (Entry<String, Meter> meter : meters.entrySet()) {
				System.out.println(String.format("%10s is %d", meter.getKey(), meter.getValue().getCount()));
			}
		}
		if (gauges.size() > 0) {
			for (Entry<String, Gauge> gauge : gauges.entrySet()) {
				Series series = new Series();
				series.setMetric(gauge.getKey());
				series.setPoints(new Object[][] { { System.currentTimeMillis() / 1000, NumberUtils.toDouble(gauge.getKey()) } });
				System.out.println(Arrays.toString(series.getPoints()[0]));
				String response = rest.postForObject(URL, series, String.class, paramsGenerator("series"));
			}
		}
	}

	private Map<String, Object> paramsGenerator(String type) {
		Map<String, Object> params = Maps.newHashMap();
		params.put("apiKey", apiKey);
		params.put("appKey", appKey);
		params.put("type", type);
		return params;
	}

	public static Builder newBuilder(MetricRegistry registry) {
		return new Builder(registry);
	}

	public static class Builder {

		private final MetricRegistry registry;
		private String appKey;
		private String apiKey;
		private String name;
		private TimeUnit rateUnit;
		private TimeUnit durationUnit;
		private MetricFilter filter;

		public Builder(MetricRegistry registry) {
			this.registry = registry;
			rateUnit = TimeUnit.SECONDS;
			durationUnit = TimeUnit.MINUTES;
			filter = MetricFilter.ALL;
			name = "app";
		}

		public Builder withAppKey(String appKey) {

			this.appKey = appKey;
			return this;
		}

		public Builder withApiKey(String apiKey) {
			this.apiKey = apiKey;
			return this;

		}

		public Builder withName(String name) {
			this.name = name;
			return this;

		}

		public Builder withRateUnit(TimeUnit rateUnit) {
			this.rateUnit = rateUnit;
			return this;

		}

		public Builder withDurationUnit(TimeUnit durationUnit) {
			this.durationUnit = durationUnit;
			return this;

		}

		public Builder withFilter(MetricFilter filter) {
			this.filter = filter;
			return this;

		}

		public DatadogMetricsReporter build() {
			return new DatadogMetricsReporter(registry, name, filter, rateUnit, durationUnit, appKey, apiKey);
		}
	}

}
