package com.littleinfinity.repository;

import static java.util.Objects.nonNull;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import com.codahale.metrics.annotation.Counted;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Gauge;
import com.codahale.metrics.annotation.Timed;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.littleinfinity.config.AppConfig;
import com.littleinfinity.request.BoundingBox;
import com.littleinfinity.response.AggregateWeatherForecast;
import com.littleinfinity.response.WeatherForecast;
import com.littleinfinity.service.Feed;

@Repository
public class FeedWeatherRepository implements WeatherRepository {

	private Feed feed;
	private AppConfig config;
	org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
	private Cache<Long, WeatherForecast> cache = CacheBuilder.newBuilder().initialCapacity(100).expireAfterWrite(30, TimeUnit.MINUTES).build();

	@Autowired
	public FeedWeatherRepository(Feed feed, AppConfig config) {
		this.feed = feed;
		this.config = config;
	}

	@Override
	public WeatherForecast updateWeatherForecast(WeatherForecast forecast) {
		if (nonNull(forecast))
			cache.put(forecast.getId(), forecast);
		else {
			throw new NullPointerException("Null forecast");
		}
		return forecast;
	}

	@Counted
	@ExceptionMetered(absolute = true, cause = RuntimeException.class, name = "invalid city id fetch")
	@Override
	public WeatherForecast getWeather(int id) {
		try {
			return doGetForecast(id);
		} catch (ExecutionException e) {
			throw new RuntimeException(e);
		}
	}

	private WeatherForecast doGetForecast(long id) throws ExecutionException {
		return cache.get(id, () -> {
			return feed.getWeather(id);
		});
	}

	@Override
	public WeatherForecast getWeather(String name) {
		return updateWeatherForecast(feed.getWeather(name));
	}

	@Override
	public List<WeatherForecast> getWeather(BoundingBox boundigBox) {
		AggregateWeatherForecast cities = feed.getCities(boundigBox);
		return feedRepository(cities.getList());
	}

	@Gauge(absolute = true, name = "cached items")
	public long cachedItems() {
		return cache.size();
	}

	@Gauge(absolute = true, name = "cache hit rate")
	public double cacheHitRate() {
		return cache.stats().hitRate();
	}

	@Timed(absolute = true, name = "FeedRefresh")
	@Scheduled(fixedDelay = 1000 * 60 * 30, initialDelay = 1000)
	public void refresh() {
		logger.info("Start refreshing repository upon configured boundingBox");
		List<WeatherForecast> results = getWeather(config.getBoundingBox());
		logger.info(String.format("Done with %d cities", results.size()));
	}

	@Override
	public Collection<WeatherForecast> getAll() {
		return cache.asMap().values();
	}

	private List<WeatherForecast> feedRepository(List<WeatherForecast> list) {
		list.forEach((forecast) -> {
			updateWeatherForecast(forecast);
		});
		return list;
	}

}
