package com.littleinfinity.repository;

import java.util.Collection;

import com.littleinfinity.request.BoundingBox;
import com.littleinfinity.response.WeatherForecast;

public interface WeatherRepository {

	WeatherForecast updateWeatherForecast(WeatherForecast forecast);

	WeatherForecast getWeather(int id);

	WeatherForecast getWeather(String name);

	Collection<WeatherForecast> getWeather(BoundingBox boundigBox);

	Collection<WeatherForecast> getAll();

}
