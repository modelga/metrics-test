package com.littleinfinity.request;

public class BoundingBox {

	private double left, down, right, up;

	private BoundingBox() {
	}

	public BoundingBox(double left, double down, double right, double up) {
		super();
		this.left = left;
		this.down = down;
		this.right = right;
		this.up = up;
	}

	public double getLeft() {
		return left;
	}

	public double getDown() {
		return down;
	}

	public void setDown(double down) {
		this.down = down;
	}

	public double getRight() {
		return right;
	}

	public void setRight(double right) {
		this.right = right;
	}

	public double getUp() {
		return up;
	}

	public void setUp(double up) {
		this.up = up;
	}

	public void setLeft(double left) {
		this.left = left;
	}

}
