package com.littleinfinity.response;

import java.util.List;

public class AggregateWeatherForecast extends Response {
	double calctime;
	List<WeatherForecast> list;

	public double getCalctime() {
		return calctime;
	}

	public void setCalctime(double calctime) {
		this.calctime = calctime;
	}

	public List<WeatherForecast> getList() {
		return list;
	}

	public void setList(List<WeatherForecast> list) {
		this.list = list;
	}
}
