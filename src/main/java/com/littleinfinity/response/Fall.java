package com.littleinfinity.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Fall {

	@JsonProperty("1h")
	double oneHour;
	@JsonProperty("3h")
	double threeHour;

	public Fall() {
		super();
	}

	public double getOneHour() {
		return oneHour;
	}

	public void setOneHour(double oneHour) {
		this.oneHour = oneHour;
	}

	public double getThreeHour() {
		return threeHour;
	}

	public void setThreeHour(double threeHour) {
		this.threeHour = threeHour;
	}

}
