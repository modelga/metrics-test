package com.littleinfinity.response;

public class Identifier {
	int type;
	int id;
	double message;
	String country;
	long sunrise;
	long suset;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getMessage() {
		return message;
	}

	public void setMessage(double message) {
		this.message = message;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long getSunrise() {
		return sunrise;
	}

	public void setSunrise(long sunrise) {
		this.sunrise = sunrise;
	}

	public long getSuset() {
		return suset;
	}

	public void setSuset(long suset) {
		this.suset = suset;
	}

}
