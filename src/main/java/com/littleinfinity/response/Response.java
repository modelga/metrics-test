package com.littleinfinity.response;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Response {

	private int cod;
	private String message;

	public Response() {
		super();
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
