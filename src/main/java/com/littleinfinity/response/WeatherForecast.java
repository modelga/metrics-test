package com.littleinfinity.response;

import java.util.List;

public class WeatherForecast extends Response {
	private long dt;
	private long id;
	private String name;
	private int log;
	private String base;
	private Wind wind;
	private Coordinates coord;
	private Identifier sys;
	private List<Weather> weather;
	private Fall rain;
	private Fall snow;
	private Cloud cloud;
	private WeatherInfo main;
	private String message;

	public long getDt() {
		return dt;
	}

	public void setDt(long dt) {
		this.dt = dt;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLog() {
		return log;
	}

	public void setLog(int log) {
		this.log = log;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Wind getWind() {
		return wind;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	public Coordinates getCoord() {
		return coord;
	}

	public void setCoord(Coordinates coord) {
		this.coord = coord;
	}

	public Identifier getSys() {
		return sys;
	}

	public void setSys(Identifier sys) {
		this.sys = sys;
	}

	public List<Weather> getWeather() {
		return weather;
	}

	public void setWeather(List<Weather> weather) {
		this.weather = weather;
	}

	public Fall getRain() {
		return rain;
	}

	public void setRain(Fall rain) {
		this.rain = rain;
	}

	public Fall getSnow() {
		return snow;
	}

	public void setSnow(Fall snow) {
		this.snow = snow;
	}

	public Cloud getCloud() {
		return cloud;
	}

	public void setCloud(Cloud cloud) {
		this.cloud = cloud;
	}

	public WeatherInfo getMain() {
		return main;
	}

	public void setMain(WeatherInfo main) {
		this.main = main;
	}

}
