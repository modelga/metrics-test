package com.littleinfinity.response;

public class Wind {
	double speed;
	int deg;
	double gust;
	int var_beg;
	int var_end;

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public int getDeg() {
		return deg;
	}

	public void setDeg(int deg) {
		this.deg = deg;
	}

	public double getGust() {
		return gust;
	}

	public void setGust(double gust) {
		this.gust = gust;
	}

	public int getVar_beg() {
		return var_beg;
	}

	public void setVar_beg(int var_beg) {
		this.var_beg = var_beg;
	}

	public int getVar_end() {
		return var_end;
	}

	public void setVar_end(int var_end) {
		this.var_end = var_end;
	}

}
