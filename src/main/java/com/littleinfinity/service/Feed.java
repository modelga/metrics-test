package com.littleinfinity.service;

import com.littleinfinity.request.BoundingBox;
import com.littleinfinity.response.AggregateWeatherForecast;
import com.littleinfinity.response.WeatherForecast;

public interface Feed {

	WeatherForecast getWeather(long id);

	WeatherForecast getWeather(String q);

	AggregateWeatherForecast getCities(BoundingBox bbox);

}
