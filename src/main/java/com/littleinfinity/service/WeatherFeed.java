package com.littleinfinity.service;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.littleinfinity.config.AppConfig;
import com.littleinfinity.request.BoundingBox;
import com.littleinfinity.response.AggregateWeatherForecast;
import com.littleinfinity.response.Response;
import com.littleinfinity.response.WeatherForecast;

@Service
public class WeatherFeed implements Feed {
	private final String URL_BASE = "http://api.openweathermap.org/data/2.5/";
	private final String WEATHER_URL_BY_ID = URL_BASE + "weather?id={id}&APPID={key}&units=metric";
	private final String WEATHER_URL_BY_STRING = URL_BASE + "weather?q={q}&APPID={key}&units=metric";
	private final String CITIES_URL_BY_BOUNDING_BOX = URL_BASE + "box/city?bbox={left},{down},{right},{up},20&APPID={key}&cluster=yes&units=metric";
	private final RestTemplate rest;
	private AppConfig config;

	@Autowired
	public WeatherFeed(AppConfig config) {
		this.config = config;
		rest = new RestTemplate();
		rest.getMessageConverters().forEach((converter) -> {
			if (converter instanceof MappingJackson2HttpMessageConverter) {
				MappingJackson2HttpMessageConverter converter2 = (MappingJackson2HttpMessageConverter) converter;
				converter2.getObjectMapper().configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
			}
		});
	}

	@Override
	public WeatherForecast getWeather(long id) {
		Map<String, Object> map = paramsFactory();
		map.put("id", id);
		WeatherForecast forecast = rest.getForObject(WEATHER_URL_BY_ID, WeatherForecast.class, map);
		return checkCOD(forecast);
	}

	@Override
	public WeatherForecast getWeather(String q) {
		Map<String, Object> map = paramsFactory();
		map.put("q", q);
		WeatherForecast forecast = rest.getForObject(WEATHER_URL_BY_STRING, WeatherForecast.class, map);
		return checkCOD(forecast);
	}

	@Override
	public AggregateWeatherForecast getCities(BoundingBox bbox) {
		Map<String, Object> map = paramsFactory();
		map.put("left", bbox.getLeft());
		map.put("down", bbox.getDown());
		map.put("right", bbox.getRight());
		map.put("up", bbox.getUp());
		AggregateWeatherForecast forecast = rest.getForObject(CITIES_URL_BY_BOUNDING_BOX, AggregateWeatherForecast.class, map);
		return checkCOD(forecast);
	}

	private <Check extends Response> Check checkCOD(Check responseToCheck) {
		if (responseToCheck.getCod() != 200)
			throw new RuntimeException(responseToCheck.getMessage());
		else
			return responseToCheck;
	}

	private Map<String, Object> paramsFactory() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("key", config.getWeatherKey());
		return params;
	}

}
