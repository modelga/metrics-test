package com.littleinfinity.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.littleinfinity.response.Response;

@ControllerAdvice
public class ControllerExceptionHandler {

	@ExceptionHandler
	public @ResponseBody Response handleConflict(HttpServletRequest req, Exception ex) {
		Response response = new Response();
		response.setCod(400);
		response.setMessage(ex.getMessage());
		return response;
	}
}
