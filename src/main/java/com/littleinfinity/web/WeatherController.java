package com.littleinfinity.web;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codahale.metrics.annotation.Metric;
import com.littleinfinity.repository.WeatherRepository;
import com.littleinfinity.request.BoundingBox;
import com.littleinfinity.response.Response;
import com.littleinfinity.response.WeatherForecast;

@Controller
public class WeatherController {

	private final WeatherRepository repository;

	@Autowired
	public WeatherController(WeatherRepository repository) {
		this.repository = repository;
	}

	@Metric(absolute = true, name = "redirections")
	@RequestMapping(value = "/")
	public void indexRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.sendRedirect(request.getContextPath() + "/index.html");
	}

	@RequestMapping(value = "/weather/{id}")
	public @ResponseBody Response getById(@PathVariable("id") int id) {
		return repository.getWeather(id);
	}

	@RequestMapping(value = "/weather/name/{name}")
	public @ResponseBody Response getById(@PathVariable("id") String name) {
		return repository.getWeather(name);
	}

	@RequestMapping(value = "/weather/box")
	public @ResponseBody Collection<WeatherForecast> getByBoundingBox(@RequestBody BoundingBox bbox) {
		return repository.getWeather(bbox);
	}

	@RequestMapping(value = "/weather/all")
	public @ResponseBody Collection<WeatherForecast> getAll() {
		return repository.getAll();
	}
}
