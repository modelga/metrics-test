package com.littleinfinity.web;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private RequestMappingHandlerAdapter adapter;

	@PostConstruct
	protected void init() {
		adapter.getMessageConverters().forEach((converter) -> {
			if (converter instanceof MappingJackson2HttpMessageConverter) {
				MappingJackson2HttpMessageConverter jackson = (MappingJackson2HttpMessageConverter) converter;
				ObjectMapper objectMapper = jackson.getObjectMapper();
				objectMapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
				objectMapper.configure(FAIL_ON_EMPTY_BEANS, false);
			}
		});

	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/*").addResourceLocations("/WEB-INF/html/");
		registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css/");
	}
}
